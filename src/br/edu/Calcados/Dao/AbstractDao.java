package br.edu.Calcados.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * Conceito de padrão de projeto DAO.
 *
 * @author Silvan, Luiz F., Barbara Priscila, Ricardo dos Anjos
 * @param <T> Tipo genérico.
 * @version 0.1, 27 de dezembro de 2012
 */
public abstract class AbstractDao<T> {

    /**
     * Operações de inserir ou atualizar.
     *
     * @param o Objeto a ser salvo (inserido ou atualizado) no banco de dados.
     * @return Código do último registro inserido ou atualizado
     */
    public abstract int salvar(T o);

    /**
     * Operação de consulta.
     *
     * @param o Modelo de objeto a ser localizado.
     * @return Lista de objetos localizados segundo modelo.
     */
    public abstract ArrayList<T> buscar(T o);

    /**
     * Operação de exclusão. <p>Deve ser verificada a real necessidade de
     * exclusão ou a necessidade de marcação do que está ativo sem excluir.</p>
     *
     * @param o Modelo de objeto a ser excluído.
     * @return Código de sucesso da exclusão.
     */
    public abstract int excluir(T o);

    /**
     * Recupera o código do último registro inserido, segundo o tipo de banco de
     * dados que está sendo utilizado.
     *
     * @return Código do último registro inserido.
     */
    public static int getUltimoId() {
        int ultimoId = 0;

        try {
            Connection con = ConectaBanco.getConexao();
            PreparedStatement pstmt;

            switch (ConectaBanco.getBanco()) {
                case ConectaBanco.POSTGRESQL:
                    pstmt = con.prepareStatement("select lastval();");
                    break;

                case ConectaBanco.SQLSERVEREXPRESS:
                    pstmt = con.prepareStatement("select scope_identity();");
                    break;

                // case ConectaBanco.MYSQL:
                default:
                    pstmt = con.prepareStatement("select last_insert_id();");
                    break;
            }

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                ultimoId = rs.getInt(1);
            }

            // Introduzia falhas em múltiplas operações consecutivas no banco de dados
//            con.close();
            pstmt.close();

        } catch (Exception ex) {
            
            JOptionPane.showMessageDialog(null, "Erro!");
        }

        return ultimoId;
    }
}
