package br.edu.Calcados.Dao;



/**
 * Fábrica de DAOs com implementação do padrão de projeto Singleton.
 *
 * @author Silvan
 * @version 0.1, 10 de janeiro de 2013
 */
public class FabricaDao {

    private static UsuarioDao usuarioDao = null;
    

   
  
    
    public static UsuarioDao novoUsuarioDao() {
        if (usuarioDao == null) {
           usuarioDao = new UsuarioDao();
        }

        return usuarioDao;
    }
    
   
    
    
}
