/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.Calcados.Dao;

import br.edu.Calcados.Classes.Usuario;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Silvan de Jesus
 */
public class Conexao {
    
    
    public Connection conexao;
    public Statement statement;
    public ResultSet resultado;
    
    
    public boolean conecta(){
        
        String driver = "com.mysql.jdbc.Driver";
        String host = "localhost";
        String usuario = "root";
        String senha = "299792458";
        String banco = "calcado";
        String url = "jdbc:mysql://" + host + ":3306/" + banco;
        
        try{
            
            Class.forName(driver);
            conexao = (Connection) DriverManager.getConnection(url, usuario, senha);
            System.out.println("Conectou!");
            return true;            
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro na conexao!");
            
            } catch(ClassNotFoundException ex){
               System.out.println("Driver Nao Encontrado!");

            }
        return false;
    }
    
    public void desconecta(){
        try{
            conexao.close();
            System.out.println("Desconectou");
        }catch(SQLException ex){
          System.out.println("Conexão Fechada!");

        }
    }
    
     public ArrayList<Usuario> buscar(Usuario o) {

        ArrayList<Usuario> f = new ArrayList<Usuario>();


        try {
            
           
            PreparedStatement pstmt;

            if (o == null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = conexao.prepareStatement("select * from usuario ;");
            }else {
                // Seleciona empregado a partir do exemplo
                pstmt = conexao.prepareStatement("select id, usuario, senha "
                        + "from usuario where usuario like ?;");
                pstmt.setString(1, o.getUsuario());
                 pstmt.setString(2, o.getSenha());
            }

            ResultSet rs = pstmt.executeQuery();//Devolve os resultado de todos os empregados do bd


            while (rs.next()) {
                f.add(new Usuario(
                        rs.getInt(1),// adciona ao arraylist o(s) novo cliente(s)
                        rs.getString(2),
                        rs.getString(3)));
                       

            }
            pstmt.close();
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        System.out.println(f);
        return f;// f é a lista de emrpegados

    }
    
   
    
    
}
