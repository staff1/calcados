/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.Calcados.Dao;


import br.edu.Calcados.Dao.Conexao;
import br.edu.Calcados.Classes.Usuario;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Silvan de Jesus
 */
public class UsuarioDao {
    
    
   
    public int salvar(Usuario o) {
        int ultimaMatricula = 0;
        try //tente
        {
            Connection con = (Connection) ConectaBanco.getConexao();
            PreparedStatement pstmt;


            // Inserir um novo objeto

            pstmt = con.prepareStatement("insert into usuario "
                    + "(usuario, senha)"
                    + "values"
                    + "(?, ?);");

            pstmt.setString(1, o.getUsuario());
            pstmt.setString(2, o.getSenha());
            



            // Reuso da variável ultimaMatricula
            pstmt.executeUpdate(); // aqui é feito a criação do usuário no banco
            ultimaMatricula = getUltimaMatricula();

            pstmt.close();

        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
            return 0;
        }
        return ultimaMatricula;
    }

    
   
    public ArrayList<Usuario> buscar(Usuario o) {

        ArrayList<Usuario> f = new ArrayList<Usuario>();


        try {
            java.sql.Connection con = ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select * from usuario ;");
            } else {
                // Seleciona empregado a partir do exemplo
                pstmt = con.prepareStatement("select usuario, id "
                        + "from usuario where usuario like ?;");
                pstmt.setString(1, o.getUsuario());
            }

            ResultSet rs = pstmt.executeQuery();//Devolve os resultado de todos os empregados do bd


            while (rs.next()) {
                f.add(new Usuario(
                        // adciona ao arraylist o(s) novo cliente(s)
                        rs.getString(1),
                        rs.getInt(2)));
                        

            }
            if(rs.first() == false)  
            {  
               //JOptionPane.showMessageDialog(null,"Nenhum funcionário Encontrado!");
               o.setAuxUsuario(0);
                              
            }
            else{
                o.setAuxUsuario(1);
            }
            
            pstmt.close();
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        System.out.println(f);
        return f;// f é a lista de emrpegados

    }
    
    
    public boolean autenticar(Usuario o) {

        Connection con = (Connection) ConectaBanco.getConexao();
        PreparedStatement pstmt = null;

        try {
            // Criação de consulta personalizada

            if (o == null) {
                return false;

            } else {
                pstmt = con.prepareStatement("select * from usuario "
                        + "where usuario = ? and senha = ? "
                        );
                pstmt.setString(1, o.getUsuario());
                pstmt.setString(2, o.getSenha());
                pstmt.setTimestamp(3, new Timestamp(new Date().getTime()));
            }

        } catch (SQLException sqlex) {
            //LogExcecao.registraExcecao(sqlex);
        }

        return realizarConsulta(pstmt) != null;
    }
    
     private ArrayList<Usuario> realizarConsulta(PreparedStatement pstmt) {

        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();

        try {
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                
                usuarios.add(
                        new Usuario(rs.getInt("id"),
                        rs.getString("usuario"),
                        rs.getString("senha")));
            }

            rs.close();
            pstmt.close();

        } catch (SQLException sqlex) {
            //LogExcecao.registraExcecao(sqlex);
        }

        if (usuarios.isEmpty()) {
            return null;

        } else {
            return usuarios;
        }
    }
     
     
     
     
      public Usuario buscarPorNome(Usuario o) {

        Connection con = (Connection) ConectaBanco.getConexao();
        PreparedStatement pstmt = null;

        try {
            // Criação de consulta personalizada

            if (o == null) {
                return null;

            } else {

                // Com restrição de limite de quantidade de retorno.
                pstmt = con.prepareStatement("select * from usuario where usuario = ?;");
                pstmt.setString(1, o.getUsuario());
            }

        } catch (SQLException sqlex) {
            //LogExcecao.registraExcecao(sqlex);
        }

        return realizarConsulta(pstmt).get(0);
    }
      
      
      public static int getUltimaMatricula() {
        int ultimaMatricula = 0;

        try {
            java.sql.Connection con = ConectaBanco.getConexao();
            PreparedStatement pstmt;

            switch (ConectaBanco.getBanco()) {
                case ConectaBanco.POSTGRESQL:
                    pstmt = con.prepareStatement("select lastval();");
                    break;

                case ConectaBanco.SQLSERVEREXPRESS:
                    pstmt = con.prepareStatement("select scope_identity();");
                    break;

                // case ConectaBanco.MYSQL:
                default:
                    pstmt = con.prepareStatement("select last_insert_id();");
                    break;
            }

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                ultimaMatricula = rs.getInt(1);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return ultimaMatricula;
    }
    
    
    
}
