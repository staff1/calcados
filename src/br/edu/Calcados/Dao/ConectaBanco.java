package br.edu.Calcados.Dao;


import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ConectaBanco {

    public static final int MYSQL = 0;//código para usar MYSQL
    public static final String MYSQLDRIVER = "com.mysql.jdbc.Driver";
    public static final String MYSQLURL =
            "jdbc:mysql://127.0.0.1:3306/calcado";
    
    public com.mysql.jdbc.Connection cone;
    public Statement statement;
    public ResultSet resultset;
    
    public static final int POSTGRESQL = 1;
    public static final String POSTGRESQLDRIVER = "org.postgresql.Driver";
    public static final String POSTGRESQLURL =
            "jdbc:postgresql://127.0.0.1:5432/empresa";
    public static final int SQLSERVEREXPRESS = 2;
    public static final String SQLSERVEREXPRESSDRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    //= "net.sourceforge.jtds.jdbc.Driver";
    private static final String SQLSERVEREXPRESSURL = "jdbc:sqlserver://127.0.0.1:1433;databaseName=empresa";
    //= "jdbc:jtds:sqlserver://127.0.0.1:1433/empresa";
    public static Connection conexao;
    public static String url;// estatica pertence a classe nao pertence a objetos.
    public static String usuario;
    public static String senha;
    public static int banco;
    
    public boolean executeSQL(String sql){
        try {
            
            statement = (Statement) conexao.createStatement();
            
            resultset = statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(ConectaBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String aUrl) {
        url = aUrl;
    }

    public static String getUsuario() {
        return usuario;
    }

    public static void setUsuario(String aUsuario) {
        usuario = aUsuario;
    }

    public static String getSenha() {
        return senha;
    }

    public static void setSenha(String aSenha) {
        senha = aSenha;
    }

    public static int getBanco() {
        return banco;
    }

    public static void setBanco(int aBanco) {
        banco = aBanco;
    }

    public ConectaBanco() {
    }

    public static void ajustaParametros(String url, String usuario,
            String senha, int banco) {
        ConectaBanco.setUrl(url);
        ConectaBanco.setUsuario(usuario);
        ConectaBanco.setSenha(senha);
        ConectaBanco.setBanco(banco);
    }

    public static Connection getConexao() {
        if (conexao == null) {
            try {
                switch (getBanco()) {
                    case MYSQL:
                        setUrl(getUrl() == null || getUrl().equals("") ? MYSQLURL : getUrl());
                        setUsuario(getUsuario() == null || getUsuario().equals("") ? "root" : getUsuario());
                        setSenha(getSenha() == null || getSenha().equals("299792458") ? "299792458" : getSenha());
                        Class.forName(MYSQLDRIVER);
                        System.out.println("Configuração MySQL bem-sucedida");
                        break;

                    case POSTGRESQL:
                        setUrl(getUrl() == null || getUrl().equals("") ? POSTGRESQLURL : getUrl());
                        setUsuario(getUsuario() == null || getUsuario().equals("") ? "postgres" : getUsuario());
                        setSenha(getSenha() == null || getSenha().equals("") ? "123456" : getSenha());
                        Class.forName(POSTGRESQLDRIVER);
                        break;

                    case SQLSERVEREXPRESS:
                        setUrl(getUrl() == null || getUrl().equals("") ? SQLSERVEREXPRESSURL : getUrl());
                        setUsuario(getUsuario() == null || getUsuario().equals("") ? "sa" : getUsuario());
                        setSenha(getSenha() == null || getSenha().equals("") ? "123456" : getSenha());
                        Class.forName(SQLSERVEREXPRESSDRIVER);
                        //Class.forName(SQLSERVEREXPRESSDRIVER);
                        break;
                }

                conexao = DriverManager.getConnection(getUrl(), getUsuario(), getSenha());

                // teste de conexão bem-sucedida

                if (conexao != null) {

                    System.out.println("Conexão bem-sucedida");

                }

            } catch (ClassNotFoundException cnfex) {
                JOptionPane.showMessageDialog(null,
                        cnfex.getMessage(), "Classe não encontrada",
                        JOptionPane.ERROR_MESSAGE);
            } catch (SQLException sqlex) {
                JOptionPane.showMessageDialog(null,
                        sqlex.getMessage(), "Erro na conexão com o banco de dados",
                        JOptionPane.ERROR_MESSAGE);
            }
        }



        return conexao;

    }

    protected static void fechaConexao() {
        url = null;
        usuario = null;
        senha = null;

        try {
            if (conexao != null) {
                conexao.close();
                conexao = null;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    protected void finalize() {
        System.out.println("ConectaBanco.finalize()");

        try {
            super.finalize();
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
        }

        fechaConexao();
    }
}
