package br.edu.Calcados.Classes;
// Generated 07/05/2013 15:59:28 by Hibernate Tools 3.2.1.GA



/**
 * Endereco generated by hbm2java
 */
public class Endereco  implements java.io.Serializable {


     private Integer id;
     private Fornecedor fornecedor;
     private Pessoas pessoas;
     private String logradouro;
     private Integer numero;
     private String bairro;
     private String complemento;
     private String cidade;
     private String uf;
     private Long cep;

    public Endereco() {
    }

    public Endereco(Fornecedor fornecedor, Pessoas pessoas, String logradouro, Integer numero, String bairro, String complemento, String cidade, String uf, Long cep) {
       this.fornecedor = fornecedor;
       this.pessoas = pessoas;
       this.logradouro = logradouro;
       this.numero = numero;
       this.bairro = bairro;
       this.complemento = complemento;
       this.cidade = cidade;
       this.uf = uf;
       this.cep = cep;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Fornecedor getFornecedor() {
        return this.fornecedor;
    }
    
    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }
    public Pessoas getPessoas() {
        return this.pessoas;
    }
    
    public void setPessoas(Pessoas pessoas) {
        this.pessoas = pessoas;
    }
    public String getLogradouro() {
        return this.logradouro;
    }
    
    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
    public Integer getNumero() {
        return this.numero;
    }
    
    public void setNumero(Integer numero) {
        this.numero = numero;
    }
    public String getBairro() {
        return this.bairro;
    }
    
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
    public String getComplemento() {
        return this.complemento;
    }
    
    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }
    public String getCidade() {
        return this.cidade;
    }
    
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    public String getUf() {
        return this.uf;
    }
    
    public void setUf(String uf) {
        this.uf = uf;
    }
    public Long getCep() {
        return this.cep;
    }
    
    public void setCep(Long cep) {
        this.cep = cep;
    }




}


